from .locators import RegisterPageLocators
from .base_page import BasePage
from selenium.webdriver.common.by import By


class RegisterPage(BasePage):
    def __init__(self, browser, url):
        super().__init__(browser, url)
        self.gender_m = self.browser.find_element(*RegisterPageLocators.GENDER_MALE)
        self.gender_f = self.browser.find_element(*RegisterPageLocators.GENDER_FEMALE)
        self.first_name_field = self.browser.find_element(*RegisterPageLocators.FIRST_NAME)
        self.last_name_field = self.browser.find_element(*RegisterPageLocators.LAST_NAME)
        self.email_field = self.browser.find_element(*RegisterPageLocators.EMAIL)
        self.password_field = self.browser.find_element(*RegisterPageLocators.PASSWORD)
        self.conf_password_field = self.browser.find_element(*RegisterPageLocators.CONFIRM_PASSWORD)
        self.register_button = self.browser.find_element(*RegisterPageLocators.REGISTER_BUTTON)

    # def define_elements_register(self):
    #     elements = {
    #         'gender_m': self.browser.find_element(*RegisterPageLocators.GENDER_MALE),
    #         'gender_f': self.browser.find_element(*RegisterPageLocators.GENDER_FEMALE),
    #         'first_name_field': self.browser.find_element(*RegisterPageLocators.FIRST_NAME),
    #         'last_name_field': self.browser.find_element(*RegisterPageLocators.LAST_NAME),
    #         'email_field': self.browser.find_element(*RegisterPageLocators.EMAIL),
    #         'password_field': self.browser.find_element(*RegisterPageLocators.PASSWORD),
    #         'conf_password_field': self.browser.find_element(*RegisterPageLocators.CONFIRM_PASSWORD),
    #         'register_button': self.browser.find_element(*RegisterPageLocators.REGISTER_BUTTON)
    #     }
    #     return elements

    def registration(self, gender, first_name, last_name, email, password):
        if gender == 'm':
            self.gender_m.click()
        elif gender == 'f':
            self.gender_f.click()
        self.first_name_field.send_keys(first_name)
        self.last_name_field.send_keys(last_name)
        self.email_field.send_keys(email)
        self.password_field.send_keys(password)
        self.conf_password_field.send_keys(password)
        self.register_button.click()
        assert self.browser.find_element(By.CSS_SELECTOR, '.result').text == 'Your registration completed'
