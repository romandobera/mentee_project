from selenium.webdriver.common.by import By


class BasePageLocators:
    REGISTER_HEADER = (By.CSS_SELECTOR, '.ico-register')
    LOGIN_HEADER = (By.CSS_SELECTOR, '.ico-login')
    CART_HEADER = (By.CSS_SELECTOR, '.ico-cart')
    WISHLIST_HEADER = (By.CSS_SELECTOR, '.ico-wishlist')
    ACCOUNT_HEADER = (By.CSS_SELECTOR, '.account')
    LOGOUT_HEADER = (By.CSS_SELECTOR, '.ico-logout')
    LOGO_HEADER = (By.CSS_SELECTOR, '.header-logo')
    HEADER_LINKS_WRAPPER = (By.CSS_SELECTOR, '.header-links-wrapper')
    SEARCH_HEADER = (By.CSS_SELECTOR, '.search-box')
    HEADER_MENU = (By.CSS_SELECTOR, '.header-menu')
    CATEGORY_NAV = (By.CSS_SELECTOR, '.block.block-category-navigationn')
    MANUFACTURER_NAV = (By.CSS_SELECTOR, '.block.block-manufacturer-navigation')
    NEWSLETTER = (By.CSS_SELECTOR, '.block.block-newsletter')
    BREADCRUMB = (By.CSS_SELECTOR, '.breadcrumb')


class RegisterPageLocators:
    GENDER_MALE = (By.CSS_SELECTOR, '#gender-male')
    GENDER_FEMALE = (By.CSS_SELECTOR, '#gender-female')
    FIRST_NAME = (By.CSS_SELECTOR, '#FirstName')
    LAST_NAME = (By.CSS_SELECTOR, '#LastName')
    EMAIL = (By.CSS_SELECTOR, '#Email')
    PASSWORD = (By.CSS_SELECTOR, '#Password')
    CONFIRM_PASSWORD = (By.CSS_SELECTOR, '#ConfirmPassword')
    REGISTER_BUTTON = (By.CSS_SELECTOR, '#register-button')


class MainPageLocators:
    pass


class ProductPageLocators:
    GALLERY = (By.CSS_SELECTOR, '.gallery')
    OVERVIEW = (By.CSS_SELECTOR, '.overview')
    PRODUCT_DESCRIPTION = (By.CSS_SELECTOR, '.full-description')
    PRODUCT_SPECS = (By.CSS_SELECTOR, '.product-specs-box')
    PRODUCT_TAGS = (By.CSS_SELECTOR, '.product-tags-box')
    ALSO_BOUGHT = (By.CSS_SELECTOR, '.also-purchased-products-grid.product-grid')
