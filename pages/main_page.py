from .locators import MainPageLocators
from .base_page import BasePage
from selenium.webdriver.common.by import By
import pytest


class MainPage(BasePage):

    def __init__(self, browser, url):
        super().__init__(browser, url)

    def define_featured_products(self):
        elements = {}
        featured_products = self.browser.find_elements(By.CSS_SELECTOR, '.picture')
        for index, product in enumerate(featured_products):
            p_item = f'product_{index}'
            elements[p_item] = product
        return elements

    def go_to_featured_product(self):
        element = self.define_featured_products()
        element['product_1'].click()

