from .base_page import BasePage
from .locators import ProductPageLocators
from .locators import BasePageLocators


class ProductPage(BasePage):
    def __init__(self, browser, url):
        super().__init__(browser, url)
        self.gallery = self.browser.find_element(*ProductPageLocators.GALLERY)
        self.overview = self.browser.find_element(*ProductPageLocators.OVERVIEW)
        self.description = self.browser.find_element(*ProductPageLocators.PRODUCT_DESCRIPTION)
        self.product_specs = self.browser.find_element(*ProductPageLocators.PRODUCT_SPECS)
        self.product_tags = self.browser.find_element(*ProductPageLocators.PRODUCT_TAGS)
        self.also_bought = self.browser.find_element(*ProductPageLocators.ALSO_BOUGHT)
        self.logo = self.browser.find_element(*BasePageLocators.LOGO_HEADER)
        self.header_links = self.browser.find_element(*BasePageLocators.HEADER_LINKS_WRAPPER)
        self.search = self.browser.find_element(*BasePageLocators.SEARCH_HEADER)
        self.menu = self.browser.find_element(*BasePageLocators.HEADER_MENU)
        self.category_nav = self.browser.find_element(*BasePageLocators.CATEGORY_NAV)
        self.manufacturer_nav = self.browser.find_element(*BasePageLocators.MANUFACTURER_NAV)
        self.newsletter = self.browser.find_element(*BasePageLocators.NEWSLETTER)
        self.breadcrumbs = self.browser.find_element(*BasePageLocators.BREADCRUMB)

    def check_for_elements_presence(self):
        assert self.newsletter
