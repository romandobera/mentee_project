from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from .locators import BasePageLocators


class BasePage:
    def __init__(self, browser, url, timeout=10):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(timeout)
        self.browser.maximize_window()

    def open(self):
        self.browser.get(self.url)

    def go_to_register_page(self):
        self.browser.find_element(*BasePageLocators.REGISTER_HEADER).click()
        assert self.browser.current_url == 'https://demowebshop.tricentis.com/register'

    def wait_for_page_reload(self):
        WebDriverWait(self.browser, 10).until(
            EC.url_changes(self.browser.current_url))

    def is_element_present(self,how, what):
        try:
            self.browser.find_element(how, what)
        except NoSuchElementException:
            return False
        return True

