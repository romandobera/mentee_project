import pytest
from datetime import datetime
from pages.base_page import BasePage
from pages.main_page import MainPage
from pages.register_page import RegisterPage
from pages.product_page import ProductPage

main_url = 'https://demowebshop.tricentis.com/'


class TestRegistration:
    @pytest.mark.parametrize('gender', ['m', 'f'])
    def test_registration(self, browser, gender):
        first_name = 'Name'
        last_name = 'Surname'
        email = f'romandoberatest+{int(datetime.now().timestamp())}@gmail.com'
        password = 'test1234'
        main_page = MainPage(browser, main_url)
        main_page.open()
        main_page.go_to_register_page()
        register_page = RegisterPage(browser, browser.current_url)
        register_page.registration(gender, first_name, last_name, email, password)


class TestPDP:
    def test_check_pdp_elements(self, browser):
        main_page = MainPage(browser,main_url)
        main_page.open()
        main_page.go_to_featured_product()
        product_page = ProductPage(browser, browser.current_url)
        product_page.check_for_elements_presence()

