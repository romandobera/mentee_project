import time
import pytest
from selenium import webdriver


@pytest.fixture(scope='function')
def browser():
    print('\n\nStart chrome browser for test...')
    browser = webdriver.Chrome()
    yield browser
    print("\nQuit browser...")
    time.sleep(10)
    browser.quit()
